# CERN CentOS 7 Docker images

http://cern.ch/linux/centos7/

## What is CERN CentOS 7 (CC7) ?

CERN Community ENTerprise Operating System 7 is the upstream CentOS 7, built to integrate into the CERN computing environment but it is not a site-specific product: all CERN site customizations are optional and can be deactivated for external users.

## Current release: CC7.7

CERN CentOS 7.7 is the current minor release of CC7.

### Image building

```
koji image-build cc7-docker-base 7.x.`date "+%Y%m%d"` \
	cc7-image-7x http://linuxsoft.cern.ch/cern/centos/7/os/x86_64 x86_64 \
	--ksurl=git+ssh://git@gitlab.cern.ch:7999/linuxsupport/cc7-base#master --kickstart=cc7-base-docker.ks  \
	--distro=RHEL-7.7 --format=docker --ksversion=RHEL7 --factory-parameter=dockerversion 1.10.1 \
	--factory-parameter=docker_env '["PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"]' \
	--factory-parameter=docker_cmd '["/bin/bash"]' \
	--scratch
```

[Tests](https://gitlab.cern.ch/linuxsupport/testing/) are in place to check everything is working as expected. If you want to skip them, run a pipeline with variable `CI_SKIP_TESTS="true"`. This can be done through CI/CD -> Pipelines -> Run pipeline -> Select branch, add the variable, press run. This will override globally defined variables in the CI.

A scheduled pipeline rebuilds the image once per month.

### Latest image
```20191109: https://koji.cern.ch/taskinfo?taskID=1603021```

```20191002: https://koji.cern.ch/taskinfo?taskID=1569095```

```20190724: https://koji.cern.ch/taskinfo?taskID=1506118```

```20181210: https://koji.cern.ch/taskinfo?taskID=1311627```

```20180516: http://koji.cern.ch/koji/taskinfo?taskID=1127545```

```20180316: http://koji.cern.ch/koji/taskinfo?taskID=1075025```

```20180112: http://koji.cern.ch/koji/taskinfo?taskID=1020938```

```20171114:  http://koji.cern.ch/koji/taskinfo?taskID=973567```

```20170920: http://koji.cern.ch/koji/taskinfo?taskID=928016```
