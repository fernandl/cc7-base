#
# CERN CentOS 7.x, based on CentOS Cloud SIG , building on tag cc7-image-7x
#
install
text
keyboard --vckeymap=us --xlayouts='us'
lang en_US.UTF-8
timezone Europe/Zurich --isUtc
network  --bootproto=dhcp --device=eth0 --onboot=on
# this is just a bogus string ...
rootpw --iscrypted --lock locked
selinux --enforcing
firewall --disabled
repo --name="CERN CentOS 7" --baseurl="http://linuxsoft.cern.ch/cern/centos/7/os/x86_64/"
repo --name="CERN CentOS 7 updates" --baseurl="http://linuxsoft.cern.ch/cern/centos/7/updates/x86_64/"
repo --name="CERN CentOS 7 extras" --baseurl="http://linuxsoft.cern.ch/cern/centos/7/extras/x86_64/"
repo --name="CERN CentOS 7 CERN" --baseurl="http://linuxsoft.cern.ch/cern/centos/7/cern/x86_64/"
#repo --name="CentOS systemd-container" --baseurl="http://linuxsoft.cern.ch/mirror/dev.centos.org/centos/7/systemd-container/"

zerombr
clearpart --all --initlabel
part / --fstype ext4 --size=3000
reboot

%packages  --excludedocs --nocore --nobase --instLangs=en
bash
#centos-release-scl
-bind-license
bind-utils
centos-release
CERN-CA-certs
cern-wrappers
cyrus-sasl-gssapi
epel-release
firewalld
-*firmware
-*firmware
-freetype
-gettext*
hepix
iproute
iputils
-kernel*
less
-libteam
openldap-clients
-os-prober
-os-prober
rootfiles
shadow-utils
-teamd
vim-minimal
yum-autoupdate
yum-plugin-ovl
-yum-firstboot

%end

%post --log=/root/ks-post.log
# randomize root password and lock root account
dd if=/dev/urandom count=50 | md5sum | passwd --stdin root
passwd -l root

# create necessary devices
/sbin/MAKEDEV /dev/console

rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-cern
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2

# some packages get installed even though we ask for them not to be,
# and they don't have any external dependencies that should make
# anaconda install them
rpm -e kernel

yum -y remove bind-libs bind-libs-lite dhclient dhcp-common dhcp-libs \
  dracut-network e2fsprogs e2fsprogs-libs ebtables ethtool file \
  firewalld freetype gettext gettext-libs groff-base grub2 grub2-tools \
  grubby initscripts iproute iptables kexec-tools libcroco libgomp \
  libmnl libnetfilter_conntrack libnfnetlink libselinux-python lzo \
  libunistring os-prober python-decorator python-slip python-slip-dbus \
  snappy sysvinit-tools which linux-firmware

yum -y install centos-release-scl

yum clean all

rm -rf /etc/firewalld
rm -rf /boot

#delete a few systemd things
rm -rf /etc/machine-id
rm -rf /usr/lib/systemd/system/multi-user.target.wants/getty.target
rm -rf /usr/lib/systemd/system/multi-user.target.wants/systemd-logind.service

# Add tsflags to keep yum from installing docs

sed -i '/distroverpkg=centos-release/a tsflags=nodocs' /etc/yum.conf

#Make it easier for systemd to run in Docker container
cp /usr/lib/systemd/system/dbus.service /etc/systemd/system/
sed -i 's/OOMScoreAdjust=-900//' /etc/systemd/system/dbus.service

#Mask mount units and getty service so that we don't get login prompt
systemctl mask systemd-remount-fs.service dev-hugepages.mount sys-fs-fuse-connections.mount systemd-logind.service getty.target console-getty.service


#Generate installtime file record
/bin/date +%Y%m%d_%H%M > /etc/BUILDTIME


#  man pages and documentation
find /usr/share/{man,doc,info,gnome/help} \
        -type f | xargs /bin/rm


#  ldconfig
rm -rf /etc/ld.so.cache
rm -rf /var/cache/ldconfig/*
rm -rf /var/cache/yum/*
rm -f /tmp/ks-script*

rm -f /usr/lib/locale/locale-archive

#Setup locale properly
localedef -v -c -i en_US -f UTF-8 en_US.UTF-8


# Create repo for systemd-container
#cat >/etc/yum.repos.d/systemd.repo <<EOF
#[systemdcontainer]
#name=CentOS-\$releasever - systemd-container
#baseurl=http://linuxsoft.cern.ch/mirror/dev.centos.org/centos/7/systemd-container/
#gpgcheck=1
#enabled=1
#gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
#
#EOF

# Clean up after the installer.
rm -f /etc/rpm/macros.imgcreate

# Fix /run/lock breakage since it's not tmpfs in docker
umount /run
systemd-tmpfiles --create --boot

############# CERN'ify ########################################################

cat > /etc/krb5.conf <<EOF
[libdefaults]
 default_realm = CERN.CH
 ticket_lifetime = 25h
 renew_lifetime = 120h
 forwardable = true
 proxiable = true
 default_tkt_enctypes = arcfour-hmac-md5 aes256-cts aes128-cts des3-cbc-sha1 des-cbc-md5 des-cbc-crc
 allow_weak_crypto = true
 chpw_prompt = true

[realms]
 CERN.CH = {
  default_domain = cern.ch
  kpasswd_server = cerndc.cern.ch
  admin_server = cerndc.cern.ch
  kdc = cerndc.cern.ch
  }

[domain_realm]
 .cern.ch = CERN.CH

pam = {
   external = true
   krb4_convert =  false
   krb4_convert_524 =  false
   krb4_use_as_req =  false
   ticket_lifetime = 25h
   use_shmem = sshd
 }

EOF

cat > /etc/openldap/ldap.conf <<EOF
#
# LDAP CERN Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE DC=cern,DC=ch
#note cerndc provides gssapi auth, xldap does not.
#HOST cerndc.cern.ch  # or xldap.cern.ch
#SIZELIMIT 12
#DEREF always

TLS_CACERTDIR /etc/openldap/certs
TLS_REQCERT demand
SSL start_tls

# Turning this off breaks GSSAPI used with krb5 when rdns = false
SASL_NOCANON	on

EOF



%end
